import '@babel/polyfill';
import svg4everybody from 'svg4everybody';
import $ from 'jquery';
import 'jquery-datetimepicker';
import 'jquery-mask-plugin';

// import fullpage from 'fullpage.js';
// import IScroll from 'fullpage.js/vendors/scrolloverflow';
//
// var fullPageInstance = new fullpage('#fullpage', {
//     scrollOverflow: true,
// });
window.$ = $;
window.jQuery = $;

svg4everybody();

require('ninelines-ua-parser');
